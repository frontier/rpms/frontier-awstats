#!/usr/bin/env python
from subprocess import *
import sys, os

def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

def run_cmd_split(cmd):
	return run_cmd(cmd).split()

def squeezeBlockAtFile( inFileName, outFileName, startMarker, endMarker):
	print 'squeezeBlockAtFile:',inFileName,outFileName,startMarker,endMarker
	file_r= open(inFileName)
	file_w= open(outFileName,'w')
	startFound=endFound=False
	for line in file_r:
		splitted=line.split()
		if startFound==False:
			if len(splitted)>0 and splitted[0]==startMarker:
				startFound=True
				file_w.write(startMarker+' 0\n')
			else:
				file_w.write(line)
		else:
			if endFound==True:
				file_w.write(line)
			else:
				if len(splitted)>0 and splitted[0]==endMarker:
					endFound=True
					file_w.write(line)
	file_w.close()
	file_r.close()
	#sys.exit(0) #For debug: run on one file only

def squeezeFile( fileName):
	outputFile= fileName+'_'
	squeezeBlockAtFile( fileName, outputFile, 'BEGIN_SIDER', 'END_SIDER')
	os.system( 'mv '+outputFile+ ' '+fileName) 

def filesOfDirToSqueeze( dir):
	cmd='find '+dir+ "| grep '.txt\|.bak'"
	print 'cmd',cmd
	return run_cmd_split( cmd)

def squeezeDir( dir):
	files= filesOfDirToSqueeze( dir)
	for file in files:
		squeezeFile( file)

def atlasDirs():
	dirs=[]
	awstatsBaseDir= '/data/home/dbfrontier/data/awstats/'
	dirsShort= run_cmd_split('ls '+awstatsBaseDir+ '|grep atlas')
	print 'dirsShort',dirsShort
	for dir in dirsShort:
		dirs.append(awstatsBaseDir+dir)
	print 'dirs',dirs
	return dirs

def squeezeAtlasDirs():
	for dir in atlasDirs():
		squeezeDir( dir)

if __name__ == '__main__':
    if len(sys.argv)==1:
		squeezeAtlasDirs()
    else:
		print 'Do not know what to do'

