#!/bin/sh

# Argument: directory to clean
# script does: rm BEGIN_SIDER entries to avoid logs to grow
if [ $# -lt 1 ] ; then
	echo "Needs one argument: directory to clean"
	exit 1
fi

AWSTATS_DATA_DIR=$1

PY_UTIL=/tmp/rm_block_util.py
cat<<EOF > $PY_UTIL
#!/usr/bin/env python
import sys
if len(sys.argv)<5:
        print 'ERROR: need 4 arguments: inFileName, outFielName, startMarker, endMarker'
        sys.exit(1)
inFileName= sys.argv[1]
outFileName= sys.argv[2]
startMarker= sys.argv[3]
endMarker= sys.argv[4]
file_r= open(inFileName)
file_w= open(outFileName,'w')
startFound=endFound=False
for line in file_r:
        splitted=line.split()
        if startFound==False:
                if len(splitted)>0 and splitted[0]==startMarker:
                        startFound=True
                        file_w.write(startMarker+' 0\n')
                else:
                        file_w.write(line)
        else:
                if endFound==True:
                        file_w.write(line)
                else:
                        if len(splitted)>0 and splitted[0]==endMarker:
                                endFound=True
                                file_w.write(line)
file_w.close()
file_r.close()
EOF

echo "AWSTATS_DATA_DIR: $AWSTATS_DATA_DIR"

#run it
chmod +x $PY_UTIL
#Avoid this month: TBD: generalize: for file in `find $AWSTATS_DATA_DIR | grep '.txt\|.bak'|grep -v awstats122`; do
for file in `find $AWSTATS_DATA_DIR | grep '.txt\|.bak'`; do
        echo "$PY_UTIL ${file} ${file}_" "BEGIN_SIDER" "END_SIDER"
        $PY_UTIL ${file} ${file}_ "BEGIN_SIDER" "END_SIDER"
        mv ${file}_ ${file}
done
rm $PY_UTIL

