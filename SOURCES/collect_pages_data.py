import os
import re
import sys

input_file = sys.argv[1]
output_file = sys.argv[2]
url_column = int(sys.argv[3])

if os.stat(input_file).st_size != 0:
    # Get new data
    input_data = {}
    with open(input_file, 'rb') as f:
        for line in f:
            values = line.split()
            http_code = values[url_column + 1].decode()
            if http_code in ("200", "304"):
                ip = values[url_column - 7].decode()
                document = values[url_column - 1].decode()
                if (ip, document) in input_data:
                    input_data[(ip, document)] += 1
                else:
                    input_data[(ip, document)] = 1

    # Get old data
    output_data = {}
    if os.path.isfile(output_file):
        with open(output_file) as f:
            for line in f:
                if not line.startswith("#"):
                    values = line.split()
                    output_data[(values[0], values[1])] = int(values[2])

    # Update data
    for key, value in input_data.items():
        if key in output_data:
            output_data[key] += input_data[key]
        else:
            output_data[key] = input_data[key]
    output_file_new = output_file + ".new"
    with open(output_file_new, "w") as f:
        f.write("# Host - URL - Hits\n")
        for key, value in output_data.items():
            f.write(key[0] + " " + key[1] + " " + str(value) + "\n")
    os.replace(output_file_new, output_file)

